<?php
/**
 * Convertion d'un fichier d'export crédit coopératif en fichier d'import pour piwam
 * 
 * @author    Leblanc Simon <contact@leblanc-simon.eu>
 * @license   Licence MIT <http://www.opensource.org/licenses/mit-license.php>
 */

// Définition des constantes
define('INPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'csv');
define('OUTPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'final');
define('ACCOUNT_NAME', 'CREDIT COOP');
define('CSV_MAX_LENGTH', 1000);
define('CSV_SEPARATOR', ',');
define('CSV_STRING', '"');

require_once __DIR__.DIRECTORY_SEPARATOR.'base.inc.php';

/**
 * Affichage de l'usage du script
 */
function usage()
{
  $script_name = pathinfo(__FILE__, PATHINFO_BASENAME);
  echo <<<EOF
Le script attend 2 arguments :
$ php $script_name "input" "output"

input doit être dans le dossier csv
output sera placé dans le dossier final

EOF;
  exit(1);
}

// Vérification du nombre d'argument
if ($argc !== 3) {
  usage();
}

// On initialise l'input et l'output
$file_input   = INPUT_DIR.DIRECTORY_SEPARATOR.$argv[1];
$file_output  = OUTPUT_DIR.DIRECTORY_SEPARATOR.$argv[2];

// On vérifie l'input
checkRequiredFile($file_input);

// On parse le fichier csv
$content = '';
while (($data = parseCsv($file_input, $handle)) !== false) {
  $content .= setInCsv(convertDate($data[0]));
  $content .= setInCsv($data[1]);
  $content .= setInCsv($data[3]);
  $content .= setInCsv(convertCurrency($data[2]));
  $content .= setInCsv(ACCOUNT_NAME);
  $content .= setInCsv('1', true);
}

// On écrit les données
if (file_put_contents($file_output, $content) === false) {
  echo '/!\ Données non écrites !'."\n";
  exit(-1);
} else {
  echo 'Données écrites dans '.$file_output."\n";
  exit(0);
}