<?php
/**
 * Convertion d'un fichier d'export paypal en fichier d'import pour piwam
 * 
 * @author    Leblanc Simon <contact@leblanc-simon.eu>
 * @license   Licence MIT <http://www.opensource.org/licenses/mit-license.php>
 */

// Définition des constantes
define('INPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'csv');
define('OUTPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'final');
define('ACCOUNT_NAME', 'PAYPAL');
define('CSV_MAX_LENGTH', 1000);
define('CSV_SEPARATOR', ',');
define('CSV_STRING', '"');

require_once __DIR__.DIRECTORY_SEPARATOR.'base.inc.php';
require_once __DIR__.DIRECTORY_SEPARATOR.'sfStringPP.php';

/**
 * Affichage de l'usage du script
 */
function usage()
{
  $script_name = pathinfo(__FILE__, PATHINFO_BASENAME);
  echo <<<EOF
Le script attend 2 arguments :
$ php $script_name "input" "output"

input doit être dans le dossier csv
output sera placé dans le dossier final

EOF;
  exit(1);
}


/**
 * Transforme un label en label et activité pour piwam
 *
 * @param   string  Le label de la ligne
 */
function processDatas($data)
{
  $date     = array();
  $label    = array();
  $activity = array();
  $currency = array();
  
  $dvd_ecole_cheque = array(
    '2NB830789K442090S',
    '8ER61055DN0353022',
    '0N603394UW693854C',
    '3GL58003VF513531G',
  );
  
  foreach ($data as $key => $value) {
    if (sfStringPP::isUtf8($value) === false) {
      $data[$key] = utf8_encode($value);
    }
  }
  
  if ($data[5] == 'Terminé' || $data[5] == 'Compensé' || $data[5] == 'Remboursé' || $data[5] == 'Partiellement remboursé') {
    // l'argent reçu
    $date[] = $data[0];
    
    switch ($data[20]) {
      // Dons récurrents
      case 'souscription1':
        $label[] = 'Don abonnement de '.$data[3];
        $activity[] = 'Dons Paypal (abonnement)';
        break;
      // Dons uniques
      case 'soutien2011':
        $label[] = 'Don de '.$data[3];
        $activity[] = 'Dons Paypal';
        break;
      // Cotisation
      case 'Cotiz2010':
      case 'Cotiz2011':
        $label[] = 'Cotisation de '.$data[3];
        $activity[] = 'Cotisation';
        break;
      default:
        switch ($data[4]) {
          // Virement vers credit crédit coop
          case 'Virement de fonds vers un compte bancaire':
            $label[] = 'Transfert vers le Crédit Cooperatif';
            $activity[] = 'Transfert sortant';
            break;
          // Dons uniques
          case 'Paiement reçu':
            $label[] = 'Don de '.$data[3].' (en direct)';
            $activity[] = 'Dons Paypal';
            break;
          // Frais d'opposition au paiement
          case 'Règlement des oppositions à des paiements':
            $label[] = 'Frais d\'opposition';
            $activity[] = 'Frais bancaire';
            break;
          // Frais d'opposition au paiement
          case 'Frais annulés':
            $label[] = 'Annulation frais d\'opposition';
            $activity[] = 'Frais bancaire';
            break;
          // Remboursement d'un don ou d'un achat
          case 'Remboursement':
            $label[] = 'Remboursement : '.$data[11];
            $activity[] = 'Remboursement vente';
            break;
          // Remboursement d'un don ou d'un achat
          case 'Annulation':
            $label[] = 'Annulation de la vente - '.$data[32];
            $activity[] = 'Remboursement vente';
            break;
          default:
            if ($data[4] == 'Paiement express envoyé' && $data[3] == 'Flattr Networks Ltd') {
              $label[] = 'Création du compte Flattr';
              $activity[] = 'Frais bancaire';
            } elseif ($data[19] == 'Don pour soutenir Framasoft') {
              $label[] = 'Don de '.$data[3];
              $activity[] = 'Dons Paypal';
            } elseif ($data[19] == 'Framasoft - Framaphonie') {
              $label[] = 'Don de '.$data[3].' pour Framaphonie';
              $activity[] = 'Dons Framaphonie';
            } elseif ($data[4] == 'Paiement du panier reçu' && $data[6] == 'Shopping Cart') {
              $label[] = 'Vente de marchandise - '.$data[14];
              $activity[] = 'Vente marchandise';
            } elseif ($data[4] == 'Paiement panier envoyé' && $data[13] == 'paypal@ubuntu-fr.org') {
              $label[] = 'Achat marchandise sur EVL - '.$data[14];
              $activity[] = 'Achat marchandise sur EVL';
            } elseif ($data[4] == 'Paiement envoyé' && $data[13] == 'paypal_avatar@framasoft.net') {
              $label[] = 'Transfert vers le compte paypal_avatar';
              $activity[] = 'Transfert sortant';
            } elseif ($data[4] == 'Paiement sur site marchand envoyé' && $data[3] == 'Réverbère sarl') {
              $label[] = 'Achat ILV';
              $activity[] = 'Achat matériel – Livres';
            } elseif ($data[4] == 'Paiement sur site marchand envoyé' && $data[3] == 'En Vente Libre' && $data[6] == 'Cotisation 2011') {
              $label[] = 'Cotisation EnVenteLibre 2011';
              $activity[] = 'Cotisation association';
            } elseif ($data[5] == 'Compensé') {
              $label[] = 'Vente de marchandise - '.$data[14];
              $activity[] = 'Vente marchandise';
            } elseif (in_array($data[14], $dvd_ecole_cheque) === true) {
              $label[] = 'Achat par procuration de DVD Ecole - '.$data[14];
              $activity[] = 'Achat marchandise par procuration';
            } else {
              throw new Exception('Erreur dans le traitement de la ligne : '.print_r($data, true));
            }
        }
    }
    
    if (preg_match('/^-?[0-9]+.[0-9]+$/', convertCurrency($data[8])) == 0) {
      throw new Exception('Erreur dans le traitement de la ligne - convertCurrency : '.convertCurrency($data[8])."\n".print_r($data, true));
    }
    
    $currency[] = $data[8];
  
    // l'argent donné généreusement à Paypal
    if ($data[9] != '0,00' && preg_match('/^-?[0-9]+,[0-9]+$/', $data[9]) != 0) {
      $date[] = $data[0];
      $label[] = 'Frais paypal';
      $activity[] = 'Frais paypal';
      $currency[] = $data[9];
    }
  } elseif ($data[5] == 'Supprimé') {
    // il s'agit d'un remboursement
    $date[] = $data[0];
    $label[] = 'Annulation de la transaction '.$data[32];
    $activity[] = 'Remboursement';
    $currency[] = $data[8];
    
    // l'argent donné généreusement à Paypal
    if ($data[9] != '0,00') {
      $date[] = $data[0];
      $label[] = 'Frais paypal';
      $activity[] = 'Frais paypal';
      $currency[] = $data[9];
    }
  } elseif ($data[5] == 'Mis en place' && $data[4] == 'Vérification du paiement') {
    // l'argent est en attente : on ne fait rien
  } elseif ($data[5] == 'Annulé') {
    // cas traité avec les remboursements
  } else {
    throw new Exception('Erreur dans le traitement de la ligne (aucun cas) : '.print_r($data, true));
  }
  
  return array($date, $label, $activity, $currency);
}

// Vérification du nombre d'argument
if ($argc !== 3) {
  usage();
}

// On initialise l'input et l'output
$file_input   = INPUT_DIR.DIRECTORY_SEPARATOR.$argv[1];
$file_output  = OUTPUT_DIR.DIRECTORY_SEPARATOR.$argv[2];

// On vérifie l'input
checkRequiredFile($file_input);

// On parse le fichier csv
$content = '';
$line = 0;
$errors = array();
while (($data = parseCsv($file_input, $handle)) !== false) {
  $line++;
  
  try {
    list($date, $label, $activity, $currency) = processDatas($data);
    $count = count($date);
    for ($i = 0; $i < $count; $i++) {
      $content .= setInCsv(convertDate($date[$i]));
      $content .= setInCsv($label[$i]);
      $content .= setInCsv($activity[$i]);
      $content .= setInCsv(convertCurrency($currency[$i]));
      $content .= setInCsv(ACCOUNT_NAME);
      $content .= setInCsv('1', true);
    }
  } catch (Exception $e) {
    $errors[] = array($e->getMessage(), $line);
  }
}

if (count($errors) > 0) {
  foreach ($errors as $error) {
    echo $error[1].' - '.$error[0]."\n*****\n";
  }
  exit(255);
}

// On écrit les données
if (file_put_contents($file_output, $content) === false) {
  echo '/!\ Données non écrites !'."\n";
  exit(-1);
} else {
  echo 'Données écrites dans '.$file_output."\n";
  exit(0);
}