<?php
/**
 * Fonction de base pour les convertion d'export-import piwam
 * 
 * @author    Leblanc Simon <contact@leblanc-simon.eu>
 * @license   Licence MIT <http://www.opensource.org/licenses/mit-license.php>
 */

/**
 * Vérification de l'existance d'un fichier requis
 *
 * @param   string  $filename   Le fichier à vérifier
 * @throws  Exception           Si le fichier n'existe pas
 */
function checkRequiredFile($filename)
{
  if (file_exists($filename) === false) {
    throw new Exception('Le fichier '.$filename.' n\'existe pas');
  }
}


/**
 * Convertion de date
 *
 * @param   string    $date   La date en anglais (yyyy-mm-dd) ou en français (dd/mm/yyyy)
 * @return  string            La date en anglais
 */
function convertDate($date)
{
  if (preg_match('#^[0-9]{1,2}/[0-9]{1,2}/[0-9]{4}$#', $date)) {
    // La date est en français
    $date = explode('/', $date);
    $res_date = $date[2].'-'.str_pad($date[1], 2, '0', STR_PAD_LEFT).'-'.str_pad($date[0], 2, '0', STR_PAD_LEFT);
  } elseif (preg_match('#^[0-9]{4}[/-][0-9]{1,2}[/-][0-9]{1,2}$#', $date)) {
    // La date est en anglais
    $date_array = explode('-', $date);
    if (count($date_array) !== 3) {
      $date_array = explode('/', $date);
      if (count($date_array) !== 3) {
        throw new Exception('La date '.$date.' est mal formée');
      }
    }
    $res_date = $date_array[0].'-'.str_pad($date_array[1], 2, '0', STR_PAD_LEFT).'-'.str_pad($date_array[2], 2, '0', STR_PAD_LEFT);
  } else {
    throw new Exception('La date '.$date.' est mal formée');
  }
  
  return $res_date;
}


/**
 * Convertion d'une chaine en valeur numérique
 *
 * @param   string  $currency   La chaine à convertir
 * @param   string              La chaine convertie
 */
function convertCurrency($currency)
{
  return str_replace(array(',', ' ', ' '), array('.', '', ''), $currency);
}


/**
 * Fonction permettant de parser un CSV
 *
 * @param   string  $filename   Le nom du fichier à parser
 * @param   handle  $handle     Le handle de parcours du fichier
 * @return  array|false         Un tableau contenant la ligne ou faux si le parcours est fini
 */
function parseCsv($filename, &$handle = null)
{
  if ($handle === null) {
    if (($handle = fopen($filename, 'rb')) === false) {
      throw new Exception('Impossible d\'ouvrir le fichier '.$filename);
    }
  }
  
  return fgetcsv($handle, CSV_MAX_LENGTH, CSV_SEPARATOR, CSV_STRING);
}


/**
 * Prépare une donnée pour le CSV
 *
 * @param   string  $data               La données à préparer
 * @param   bool    $is_eof             Vrai si c'est le dernier élément de la ligne
 * @param   string  $string_separator   Le séparateur de chaine
 * @param   string  $col_separator      Le séparateur de colonne
 * @return  string                      La donnée formatée pour le CSV
 */
function setInCsv($data, $is_eof = false, $string_separator = '"', $col_separator = ',')
{
  $data = trim($data);
  
  if ($string_separator === '"') {
    $data = str_replace('"', '""', $data);
  }
  
  $data = $string_separator.$data.$string_separator;
  
  if ($is_eof === true) {
    return $data."\n";
  } else {
    return $data.$col_separator;
  }
}