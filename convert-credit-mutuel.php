<?php
/**
 * Convertion d'un fichier d'export crédit mutuel en fichier d'import pour piwam
 * 
 * @author    Leblanc Simon <contact@leblanc-simon.eu>
 * @license   Licence MIT <http://www.opensource.org/licenses/mit-license.php>
 */

// Définition des constantes
define('INPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'csv');
define('OUTPUT_DIR', __DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'final');
define('ACCOUNT_NAME', 'CREDIT MUT');
define('CSV_MAX_LENGTH', 1000);
define('CSV_SEPARATOR', ',');
define('CSV_STRING', '"');

require_once __DIR__.DIRECTORY_SEPARATOR.'base.inc.php';

/**
 * Affichage de l'usage du script
 */
function usage()
{
  $script_name = pathinfo(__FILE__, PATHINFO_BASENAME);
  echo <<<EOF
Le script attend 2 arguments :
$ php $script_name "input" "output"

input doit être dans le dossier csv
output sera placé dans le dossier final

EOF;
  exit(1);
}


/**
 * Transforme un label en label et activité pour piwam
 *
 * @param   string  Le label de la ligne
 */
function labelToLabelAndActivity($data)
{
  if (preg_match('/TPE [0-9]{4}$/', $data) > 0) {
    $label = 'Don reçu';
    $activity = 'Dons';
  } elseif (preg_match('/TPE [0-9]{4} CION$/', $data) > 0) {
    $label = 'Frais de transaction CB';
    $activity = 'Frais bancaire';
  } elseif (substr($data, 0, 12) == 'E I FACT TPE' || substr($data, 0, 13) == 'E.I FACT. TPE') {
    $label = 'Facturation des TPE virtuel';
    $activity = 'Frais bancaire';
  } elseif (substr($data, 0, 11) == 'FACTURE SGT') {
    $label = 'Facturation du compte Crédit Mutuel';
    $activity = 'Frais bancaire';
  } elseif (substr($data, 0, 31) == 'AVOIR COTIS EUROCOMPTE ASSOCIA.') {
    $label = 'Remboursement facturation du compte Crédit Mutuel';
    $activity = 'Frais bancaire';
  } elseif (substr($data, 0, 7) == 'REM CHQ') {
    $label = 'Remise de chèques';
    $activity = 'Remise de chèques';
  } elseif ($data == 'VIR TRANSFERT VERS CREDIT COOP') {
    $label = 'Transfert vers le Crédit Cooperatif';
    $activity = 'Transfert sortant';
  } elseif ($data == 'VIR FRAMASOFT APPROV CR DIT MUTUEL') {
    $label = 'Transfert du Crédit Cooperatif';
    $activity = 'Transfert entrant';
    
  //
  // Cas particuliers
  //
  } elseif (preg_match('/CHEQUE ([0-9]+) T[0-9-]+/', $data, $matches) > 0) {
    switch ($matches[1]) {
      case '1211241':
        $label = 'Chèque du 04/12/2010 - Odile Martinez : remboursement d\'un don';
        $activity = 'Dons';
        break;
      case '1211242':
        $label = 'Chèque du 30/03/2011 - MPO : achat de DVD';
        $activity = 'Achat matériel – DVD';
        break;
      case '1211243':
        $label = 'Chèque du 27/05/2011 - Foyer Etudiant Catholique : Logement RMLL';
        $activity = 'RMLL';
        break;
      case '1211244':
        $label = 'Chèque du 11/07/2011 - Foyer Etudiant Catholique : Logement RMLL';
        $activity = 'RMLL';
        break;
      default:
        throw new Exception('Erreur dans le traitement de la ligne : '.$data);
    }
  } elseif ($data == 'CONVENTION DE PARTENARIAT') {
    $label = 'Signature d\'une convention de partenariat';
    $activity = 'Mécénat';
  } elseif ($data == 'VIR LAURES CONSEIL') {
    $label = 'Don de Laures Conseil';
    $activity = 'Dons';
  } elseif ($data == 'VIR 50FK - CMD 011/DISKCARD/001') {
    $label = 'Achat Clés USB – Diskcard (011/DISKCARD/001)';
    $activity = 'Achat matériel – clé USB';
  } elseif ($data == 'VIR ATRAMENTA 0084-24251-0192') {
    $label = 'Achat livres Atramenta (0084-24251-0192)';
    $activity = 'Achat matériel – Livres';
  } elseif ($data == 'VRST REF02723B00') {
    $label = 'Virement VRST REF02723B00 d\'origine inconnue';
    $activity = 'Inconnu';
    
  //
  // Erreur
  //
  } else {
    throw new Exception('Erreur dans le traitement de la ligne : '.$data);
  }
  
  return array($label, $activity);
}

function currencyFromCreditOrDebit($credit, $debit)
{
  if ($credit == '0') {
    if ($debit == '0') {
      throw new Exception('le credit et le debit sont égals à 0');
    }
    
    return '-'.$debit;
  } else {
    return $credit;
  }
}

// Vérification du nombre d'argument
if ($argc !== 3) {
  usage();
}

// On initialise l'input et l'output
$file_input   = INPUT_DIR.DIRECTORY_SEPARATOR.$argv[1];
$file_output  = OUTPUT_DIR.DIRECTORY_SEPARATOR.$argv[2];

// On vérifie l'input
checkRequiredFile($file_input);

// On parse le fichier csv
$content = '';
$line = 0;
$errors = array();
while (($data = parseCsv($file_input, $handle)) !== false) {
  $line++;
  
  try {
    $content .= setInCsv(convertDate($data[3])); // 3
    list($label, $activity) = labelToLabelAndActivity($data[8]);
    $content .= setInCsv($label); // 8
    $content .= setInCsv($activity); // 8
    $content .= setInCsv(convertCurrency(currencyFromCreditOrDebit($data[9], $data[10]))); //9 ou 10 (-)
    $content .= setInCsv(ACCOUNT_NAME);
    $content .= setInCsv('1', true);
  } catch (Exception $e) {
    $errors[] = array($e->getMessage(), $line);
  }
}

if (count($errors) > 0) {
  foreach ($errors as $error) {
    echo $error[1].' - '.$error[0]."\n*****\n";
  }
  exit(255);
}

// On écrit les données
if (file_put_contents($file_output, $content) === false) {
  echo '/!\ Données non écrites !'."\n";
  exit(-1);
} else {
  echo 'Données écrites dans '.$file_output."\n";
  exit(0);
}